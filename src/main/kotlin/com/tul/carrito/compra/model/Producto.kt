package com.tul.carrito.compra.model

import sun.security.krb5.internal.ccache.Tag
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

@Entity
@Table(name = "producto")
open class Producto{

    @get:Id
    @get:GeneratedValue
    @get:Column(name = "id")
    open var id: Long? = null

    @get:PrimaryKeyJoinColumns
    @get:Column(name = "uuid")
    open var uuid: String? = null

    @get:Column(name = "nombre")
    open var nombre: String? = null

    @get:Column(name = "sku")
    open var sku: String? = null

    @get:Column(name = "descripcion")
    open var descripcion: String? = null

    @get:Column(name = "precio")
    open var precio: Long? = null

    @get:Column(name = "tipo")
    open var tipo: String? = null

    @get:Column(name = "cantidad")
    open var cantidad: Long? = null

    @get:ManyToMany(mappedBy = "producto")
    var carrito: Set<Carrito> = HashSet()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as Producto
        if (uuid != that.uuid) return false
        return true
    }

    override fun hashCode(): Int {
        return if (uuid != null)
            uuid.hashCode()
        else 0
    }

}