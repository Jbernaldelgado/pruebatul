package com.tul.carrito.compra.model

import com.tul.carrito.compra.enum.Estado
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

@Entity
@Table(name = "carrito")
open class Carrito {

    @get:Id
    @get:GeneratedValue
    @get:Column(name = "id")
    open var id: Long? = null

    @get:PrimaryKeyJoinColumns
    @get:Column(name = "uuid")
    open var uuid: String? = null

    @get:Column(name = "total")
    open var total: Long? = null

    @get:Column(name = "estado")
    open var estado: Estado? = null

    @get:ManyToMany
    @get:JoinTable(
            name = "carrito_producto",
            joinColumns = [JoinColumn(name = "carrito_id")],
            inverseJoinColumns = [JoinColumn(name = "producto_id")]
    )
    open var producto: Set<Producto> = HashSet()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as Carrito
        if (uuid != that.uuid) return false
        return true
    }

    override fun hashCode(): Int {
        return if (uuid != null)
            uuid.hashCode()
        else 0
    }


}