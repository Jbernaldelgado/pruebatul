package com.tul.carrito.compra.business

import com.tul.carrito.compra.dao.CarritoRepository
import com.tul.carrito.compra.exception.BusinessException
import com.tul.carrito.compra.exception.NotFoundException
import com.tul.carrito.compra.model.Carrito
import com.tul.carrito.compra.model.Producto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.jvm.Throws

@Service
class CarritoBusiness: ICarritoBusiness {

    @Autowired
    val carritoRepository: CarritoRepository? = null

    @Throws(BusinessException::class)
    override fun list(): List<Carrito> {
        try {
            return carritoRepository!!.findAll()
        }catch (e:Exception){
            throw BusinessException(e.message)
        }
    }

    @Throws(BusinessException::class, NotFoundException::class)
    override fun load(idCarrito: Long): Carrito {
        val op: Optional<Carrito>
        try {
            op = carritoRepository!!.findById(idCarrito)
        }catch (e: java.lang.Exception){
            throw BusinessException(e.message)
        }
        if(!op.isPresent){
            throw NotFoundException("No se encuentra el carrito con id $idCarrito")
        }
        return op.get()
    }

    @Throws(BusinessException::class)
    override fun save(carrito: Carrito): Carrito {
        try {
            return carritoRepository!!.save(carrito)
        }catch (e: java.lang.Exception){
            throw BusinessException(e.message)
        }
    }

    override fun remove(idCarrito: Long) {
        val op:Optional<Carrito>
        try {
            op = carritoRepository!!.findById(idCarrito)
        }catch (e: java.lang.Exception){
            throw BusinessException(e.message)
        }
        if(!op.isPresent){
            throw NotFoundException("No se encuentra el producto con id $idCarrito")
        }else{
            try {
                carritoRepository!!.deleteById(idCarrito)
            }catch (e: java.lang.Exception){
                throw BusinessException(e.message)
            }
        }
    }
}