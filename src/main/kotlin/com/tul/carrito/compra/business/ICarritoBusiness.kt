package com.tul.carrito.compra.business

import com.tul.carrito.compra.model.Carrito

interface ICarritoBusiness {

    fun list(): List<Carrito>
    fun load(idCarrito:Long): Carrito
    fun save(carrito: Carrito): Carrito
    fun remove(idCarrito: Long)
}