package com.tul.carrito.compra.business

import com.tul.carrito.compra.dao.ProductoRepository
import com.tul.carrito.compra.exception.BusinessException
import com.tul.carrito.compra.exception.NotFoundException
import com.tul.carrito.compra.model.Producto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.Exception
import java.util.*
import kotlin.jvm.Throws

@Service
class ProductoBusiness: IProductoBusiness {

    @Autowired
    val productoRepository : ProductoRepository? = null

    @Throws(BusinessException::class)
    override fun list(): List<Producto> {
        try {
            return productoRepository!!.findAll()
        }catch (e:Exception){
            throw BusinessException(e.message)
        }
    }

    @Throws(BusinessException::class, NotFoundException::class)
    override fun load(idProducto: Long): Producto {
        val op:Optional<Producto>
        try {
            op = productoRepository!!.findById(idProducto)
        }catch (e:Exception){
            throw BusinessException(e.message)
        }
        if(!op.isPresent){
            throw NotFoundException("No se encuentra el producto con id $idProducto")
        }
        return op.get()
    }

    @Throws(BusinessException::class)
    override fun save(producto: Producto): Producto {
        try {
            return productoRepository!!.save(producto)
        }catch (e:Exception){
            throw BusinessException(e.message)
        }
    }

    override fun remove(idProducto: Long) {
        val op:Optional<Producto>
        try {
            op = productoRepository!!.findById(idProducto)
        }catch (e:Exception){
            throw BusinessException(e.message)
        }
        if(!op.isPresent){
            throw NotFoundException("No se encuentra el producto con id $idProducto")
        }else{
            try {
                productoRepository!!.deleteById(idProducto)
            }catch (e:Exception){
                throw BusinessException(e.message)
            }
        }
    }
}