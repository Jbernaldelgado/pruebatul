package com.tul.carrito.compra.dao

import com.tul.carrito.compra.model.Producto
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductoRepository: JpaRepository<Producto,Long> {
}