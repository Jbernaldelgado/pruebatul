package com.tul.carrito.compra.dao

import com.tul.carrito.compra.model.Carrito
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CarritoRepository: JpaRepository<Carrito, Long> {
}