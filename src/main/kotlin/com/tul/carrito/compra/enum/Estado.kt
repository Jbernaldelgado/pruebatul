package com.tul.carrito.compra.enum

enum class Estado(val valor:String) {
    PENDIENTE("P"),
    COMPLETADO("C");
}