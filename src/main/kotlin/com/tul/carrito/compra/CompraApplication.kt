package com.tul.carrito.compra

import com.tul.carrito.compra.dao.CarritoRepository
import com.tul.carrito.compra.dao.ProductoRepository
import com.tul.carrito.compra.model.Carrito
import com.tul.carrito.compra.model.Producto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CompraApplication:CommandLineRunner{

	@Autowired
	val productoRepository: ProductoRepository? = null

	@Autowired
	val carritoRepository: CarritoRepository? = null

	override fun run(vararg args: String?){

	}
}

fun main(args: Array<String>) {
	runApplication<CompraApplication>(*args)
}
