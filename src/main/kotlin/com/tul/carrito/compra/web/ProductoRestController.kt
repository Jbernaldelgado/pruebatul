package com.tul.carrito.compra.web

import com.tul.carrito.compra.business.IProductoBusiness
import com.tul.carrito.compra.exception.BusinessException
import com.tul.carrito.compra.exception.NotFoundException
import com.tul.carrito.compra.model.Producto
import com.tul.carrito.compra.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import sun.rmi.runtime.Log
import java.lang.Exception
import java.util.*

@RestController
@RequestMapping(Constants.URL_API_BASE)
class ProductoRestController {

    @Autowired
    val productoBusiness : IProductoBusiness? = null

    @GetMapping("/productos")
    fun list(): ResponseEntity<List<Producto>>{
        return try {
            ResponseEntity(productoBusiness!!.list(), HttpStatus.OK)
        }catch (e: Exception){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("productos/{id}")
    fun load(@PathVariable("id") idProducto:Long): ResponseEntity<Producto>{
        return try {
            ResponseEntity(productoBusiness!!.load(idProducto), HttpStatus.OK)
        }catch (e:BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }catch (e:NotFoundException){
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping("productos")
    fun insert(@RequestBody producto:Producto): ResponseEntity<Any>{
        return try {
            val uuidGenerado = UUID.randomUUID().toString();
            producto.uuid = uuidGenerado
            productoBusiness!!.save(producto)
            val responseHeader = HttpHeaders()
            responseHeader.set("location",Constants.URL_API_BASE + "/productos/" + producto.uuid)
            ResponseEntity(responseHeader, HttpStatus.CREATED)
        }catch (e:BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PutMapping("productos")
    fun update(@RequestBody producto: Producto): ResponseEntity<Any>{
        return try {
            productoBusiness!!.save(producto)
            ResponseEntity(HttpStatus.OK)
        }catch (e: BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @DeleteMapping("productos/{id}")
    fun delete(@PathVariable("id") idProducto:Long): ResponseEntity<Any>{
        return try {
            productoBusiness!!.remove(idProducto)
            ResponseEntity(HttpStatus.OK)
        }catch (e:BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }catch (e:NotFoundException){
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

}