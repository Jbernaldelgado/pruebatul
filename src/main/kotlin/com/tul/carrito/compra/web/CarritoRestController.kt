package com.tul.carrito.compra.web

import com.tul.carrito.compra.business.ICarritoBusiness
import com.tul.carrito.compra.enum.Estado
import com.tul.carrito.compra.exception.BusinessException
import com.tul.carrito.compra.exception.NotFoundException
import com.tul.carrito.compra.model.Carrito
import com.tul.carrito.compra.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.Exception
import java.util.*

@RestController
@RequestMapping(Constants.URL_API_BASE)
class CarritoRestController {

    @Autowired
    val carritoBusiness: ICarritoBusiness? = null

    @GetMapping("/carritos")
    fun list(): ResponseEntity<List<Carrito>> {
        return try {
            ResponseEntity(carritoBusiness!!.list(), HttpStatus.OK)
        }catch (e: Exception){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("carritos/{id}")
    fun load(@PathVariable("id") idCarrito:Long): ResponseEntity<Carrito> {
        return try {
            ResponseEntity(carritoBusiness!!.load(idCarrito), HttpStatus.OK)
        }catch (e: BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }catch (e: NotFoundException){
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping("carritos")
    fun insert(@RequestBody carrito: Carrito): ResponseEntity<Any> {
        return try {
            var totalFinal:Long =0
            for (valor in carrito.producto){
                if(valor.tipo.equals("S")){
                    totalFinal += valor.precio!!
                }else{
                    totalFinal += (valor.precio!!/2)
                }
            }
            carrito.total = totalFinal
            val uuidGenerado = UUID.randomUUID().toString();
            carrito.uuid = uuidGenerado
            carritoBusiness!!.save(carrito)
            val responseHeader = HttpHeaders()
            responseHeader.set("location",Constants.URL_API_BASE + "/carritos/" + carrito.uuid)
            ResponseEntity(responseHeader, HttpStatus.CREATED)
        }catch (e: BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PutMapping("carritos")
    fun update(@RequestBody carrito: Carrito): ResponseEntity<Any> {
        return try {
            carritoBusiness!!.save(carrito)
            ResponseEntity(HttpStatus.OK)
        }catch (e: BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PutMapping("carritos/checkout")
    fun Checkout(@RequestBody carrito: Carrito): ResponseEntity<Any> {
        return try {
            carrito.estado = Estado.COMPLETADO;
            carritoBusiness!!.save(carrito)
            val responseHeader = HttpHeaders()
            responseHeader.add("costo final:",carrito.total.toString())
            ResponseEntity(responseHeader, HttpStatus.OK)
        }catch (e: BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @DeleteMapping("carritos/{id}")
    fun delete(@PathVariable("id") idCarrito:Long): ResponseEntity<Any> {
        return try {
            carritoBusiness!!.remove(idCarrito)
            ResponseEntity(HttpStatus.OK)
        }catch (e: BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }catch (e: NotFoundException){
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }
}