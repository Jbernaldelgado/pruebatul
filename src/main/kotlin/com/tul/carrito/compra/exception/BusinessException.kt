package com.tul.carrito.compra.exception

import java.lang.Exception

class BusinessException(message:String?): Exception(message) {

}