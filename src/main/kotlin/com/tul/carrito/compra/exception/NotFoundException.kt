package com.tul.carrito.compra.exception

import java.lang.Exception

class NotFoundException(message:String?):Exception(message) {
}